/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef SAMPLEAPPLICATION_H_
#define SAMPLEAPPLICATION_H_

#include <nox/common/types.h>
#include <nox/util/process/Manager.h>
#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/Logic.h>
#include <nox/logic/View.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/Timer.h>
#include "GameWindow.h"

class GameApplication final: public nox::app::SdlApplication
{
public:
	GameApplication();

private:
	bool onInit() override;
	void onDestroy() override;
	void onUpdate(const nox::Duration& deltaTime) override;
	void onSdlEvent(const SDL_Event& event) override;

	bool initializeResourceCache();
	bool initializeDataStorage();
	bool initializeAudio();
	bool initializeLogic();
	bool initializeWindow();

	bool loadWorld(const std::string& worldPath);
	void playTestSound();

	nox::app::log::Logger logger;
	nox::util::Timer<nox::Duration> tpsUpdateTimer;
	nox::process::Manager processManager;
	nox::logic::event::ListenerManager listener;

	nox::logic::Logic* logic;
	nox::logic::world::Manager* world;
	GameWindow* window;
};

#endif /* TESTGAMEAPPLICATION_H_ */
