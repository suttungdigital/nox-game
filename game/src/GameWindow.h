/*
 * NOX Engine
 *
 * Copyright (c) 2015 Suttung Digital AS (suttungdigital.com)
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

#ifndef NOXRENDERWINDOWVIEW_H_
#define NOXRENDERWINDOWVIEW_H_

#include <nox/app/graphics/2d/Camera.h>
#include <nox/app/graphics/2d/TransformationNode.h>
#include <nox/window/RenderSdlWindowView.h>
#include <nox/window/SdlKeyboardControlMapper.h>
#include <nox/logic/event/IBroadcaster.h>
#include <nox/logic/actor/Actor.h>

class GameWindow final: public nox::window::RenderSdlWindowView
{
public:
	GameWindow(nox::app::IContext* applicationContext, const std::string& windowTitle);

private:
	void onRendererCreated(nox::app::graphics::IRenderer* renderer) override;
	void onEvent(const std::shared_ptr<nox::logic::event::Event>& event) override;

	bool initialize(nox::logic::IContext* context) override;
	void onWindowSizeChanged(const glm::uvec2& size) override;
	void onMousePress(const SDL_MouseButtonEvent& event) override;
	void onMouseRelease(const SDL_MouseButtonEvent& event) override;
	void onMouseMove(const SDL_MouseMotionEvent& event) override;
	void onMouseScroll(const SDL_MouseWheelEvent& event) override;
	void onKeyPress(const SDL_KeyboardEvent& event) override;
	void onKeyRelease(const SDL_KeyboardEvent& event) override;
	void onControlledActorChanged(nox::logic::actor::Actor* controlledActor);

	glm::vec2 convertMouseToWorld(const glm::ivec2& mousePos) const;

	nox::app::log::Logger log;
	nox::logic::event::ListenerManager listener;
	nox::window::SdlKeyboardControlMapper controlMapper;

	nox::logic::event::IBroadcaster* eventBroadcaster;

	nox::app::graphics::IRenderer* renderer;
	std::shared_ptr<nox::app::graphics::Camera> camera;
	std::shared_ptr<nox::app::graphics::TransformationNode> rootSceneNode;

	int mouseJointId;

	bool cameraPanning;
	float cameraZoomSpeed;
};

#endif
