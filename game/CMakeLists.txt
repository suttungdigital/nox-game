get_filename_component(SAMPLE_NAME "${CMAKE_CURRENT_LIST_DIR}" NAME)
project(${SAMPLE_NAME})

set(SOURCES
	src/main.cpp
	src/GameApplication.h
	src/GameApplication.cpp
	src/GameWindow.h
	src/GameWindow.cpp
)

add_executable(${SAMPLE_NAME} ${SOURCES})
target_link_libraries(${SAMPLE_NAME} ${GAME_NOX_LIBRARY})
