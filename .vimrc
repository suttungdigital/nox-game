" So that gf will work with #include paths
let &path.="nox-engine/include,nox-engine/src"

" Header/source switching with fswitch
au! BufEnter *.cpp let b:fswitchdst = 'h' | let b:fswitchlocs = 'reg:\src\include/nox\'
au! BufEnter *.h let b:fswitchdst = 'cpp' | let b:fswitchlocs = 'reg:\include/nox\src\'

if filereadable(".vimcustomrc")
	source .vimcustomrc
endif
